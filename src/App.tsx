import React from "react";
import { Routes, Route } from "react-router-dom";
import { Error404Page } from "./pages/Error404Page";
import { LoginPage } from "./pages/LoginPage";
import { MainPage } from "./pages/MainPage";
import { ResourcePage } from "./pages/ResourcePage";
import { getToken, setToken } from "./TokenService";

export function App(): JSX.Element {
	const [counter, setCounter] = React.useState<number>(0);

	const token: string | undefined = getToken();

	function handleTokenChange(token: string): void {
		setToken(token);
		setCounter(counter + 1);
	}

    return <div className="App">
      <Routes>
		<Route path="/" element={<MainPage token={token}/>}/>
		<Route path="/login" element={<LoginPage onTokenChange={handleTokenChange}/>}/>
		<Route path="/:resource/*" element={<ResourcePage token={token}/>}/>
		<Route path="/404" element={<Error404Page/>}/>
	  </Routes>
    </div>;
}

export default App;
