import Button from '@mui/material/Button';
import { Link } from 'react-router-dom';
import ArrowBackIosIcon from '@mui/icons-material/ArrowBackIos';

export interface BackButtonProps {
    to: string;
}

export function BackButton(props: BackButtonProps): JSX.Element {
    return <div>
        <Button
            variant="outlined"
            component={Link}
            to={props.to}
            startIcon={<ArrowBackIosIcon/>}
        >Back</Button>
    </div>;
}