import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { useNavigate, NavigateFunction, Link } from "react-router-dom";
import React from 'react';
import axios, { AxiosResponse } from 'axios';
import { APICollectionResponse } from '../interfaces/APICollectionResponse';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select, { SelectChangeEvent } from '@mui/material/Select';
import Button from '@mui/material/Button';
import './PeopleListPage.css';
import { BackButton } from '../components/BackButton';
import IconButton from '@mui/material/IconButton';
import DeleteOutlinedIcon from '@mui/icons-material/DeleteOutlined';
import AddIcon from '@mui/icons-material/Add';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';

export interface ResourceListPageProps {
    resource: string;
    token?: string;
}

export function ResourceListPage<R extends { id: string | number }>(props: ResourceListPageProps): JSX.Element {
    const [people, setPeople] = React.useState<R[]>([]);
    const [perPage, setPerPage] = React.useState<number>(6);
    const [pages, setPages] = React.useState<number>(2);
    const [page, setPage] = React.useState<number>(1);
    const [removing, setRemoving] = React.useState<R | undefined>(undefined);

    const navigate: NavigateFunction = useNavigate();

    React.useEffect((): void => {
        axios.get(`https://reqres.in/api/${props.resource}?per_page=${perPage}&page=${page}`)
            .then((response: AxiosResponse<APICollectionResponse<R[]>>): void => {
                setPeople(response.data.data);
                setPages(response.data.total_pages);
                setPage(Math.min(page, response.data.total_pages));
            });
    }, [perPage, page]);

    function handleChange(event: SelectChangeEvent<number>): void {
        setPerPage(event.target.value as number);
        setPeople([]);
    }

    function handleDeleteClick(event: React.MouseEvent, person: R): void {
        event.stopPropagation();
        setRemoving(person);
    }

    function handleModalClose(): void {
        setRemoving(undefined);
    }

    function handleModalAccept(): void {
        setPeople(people.filter((p: R): boolean => p !== removing));
        handleModalClose();
    }

    return <div className='PeopleListPage'>
        <BackButton to='/'/>
        <TableContainer component={Paper}>
            <Table sx={{ minWidth: 650 }} aria-label="simple table">
                <TableHead>
                <TableRow>
                    <TableCell>ID</TableCell>
                    {people[0] && Object.keys(people[0])
                        .filter((key: string): boolean => key !== "id")
                        .map((key: string): JSX.Element => <TableCell key={key}>{key}</TableCell>)
                    }
                    <TableCell></TableCell>
                </TableRow>
                </TableHead>
                <TableBody>
                    {people.map((person: R): JSX.Element => 
                        <TableRow onClick={() => navigate(`/${props.resource}/${person.id}`)} key={person.id} className="person">
                            <TableCell component="th" scope="row">{person.id}</TableCell>
                            {Object.keys(person)
                                .filter((key: string): boolean => key !== "id")
                                // @ts-ignore
                                .map((key: string): JSX.Element => <TableCell key={key}>{person[key]}</TableCell>)
                            }
                            <TableCell>
                                {props.token && <IconButton
                                    color="error"
                                    component="span"
                                    onClick={(event: React.MouseEvent): void => handleDeleteClick(event, person)}
                                >
                                    <DeleteOutlinedIcon/>
                                </IconButton>}
                            </TableCell>
                        </TableRow>
                    )}
                </TableBody>
            </Table>
        </TableContainer>
        <FormControl fullWidth>
        <InputLabel id="per-page-select-label">Items per page</InputLabel>
        <Select
            labelId="per-page-select-label"
            id="per-page-select"
            value={perPage}
            label="Items per page"
            onChange={handleChange}
        >
            {Array(12)
                .fill(1)
                .map((element: number, index: number): number => element + index)
                .map((element: number): JSX.Element => 
                    <MenuItem key={element} value={element}>{element}</MenuItem>
                )}
        </Select>
        </FormControl>
        <Button variant="outlined" onClick={() => setPage(1)} disabled={page === 1}>&lt;&lt;</Button>
        <Button variant="outlined"  onClick={() => setPage(page - 1)} disabled={page === 1}>&lt;</Button>
            {Array(pages)
                .fill(1)
                .map((element: number, index: number): number => element + index)
                .map((element: number): JSX.Element => 
                    <Button
                        key={element}
                        variant="outlined"
                        onClick={() => setPage(element)}
                        disabled={page === element}
                    >{element}</Button>)}
        <Button variant="outlined" onClick={() => setPage(page + 1)} disabled={page === pages}>&gt;</Button>
        <Button variant="outlined" onClick={() => setPage(pages)} disabled={page === pages}>&gt;&gt;</Button>
        {props.token && <div>
            <Button variant="contained" component={Link} to={`/${props.resource}/add`} endIcon={<AddIcon/>}>Add person</Button>
        </div>}
        <Dialog open={removing !== undefined} onClose={handleModalClose}>
            <DialogTitle>Are you sure?</DialogTitle>
            <DialogContent>
                <DialogContentText>
                    {`You are going to remove "${removing?.id}". Are you sure you want to proceed?`}
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button onClick={handleModalClose}>Cancel</Button>
                <Button onClick={handleModalAccept} autoFocus>Accept</Button>
            </DialogActions>
        </Dialog>
    </div>;
}
