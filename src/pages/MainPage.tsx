import { Link, useNavigate, NavigateFunction } from "react-router-dom";
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import SendIcon from '@mui/icons-material/Send';
import React from "react";
import ButtonGroup from '@mui/material/ButtonGroup';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';

export interface MainPageProps {
    readonly token?: string;
}

export function MainPage(props: MainPageProps): JSX.Element {
    const [value, setValue] = React.useState<string>("");
    const [buttons, setButtons] = React.useState<string[]>(['users']);
    const [error, setError] = React.useState<boolean>(false);
    const [removing, setRemoving] = React.useState<string | undefined>(undefined);

    const navigate: NavigateFunction = useNavigate();

    function handleChange(event: React.ChangeEvent<HTMLInputElement>): void {
        setValue(event.target.value as string);
    }

    function handleAdd(): void {
        setError(false);
        if (buttons.includes(value)) {
            setError(true);
        } else {
            setButtons([...buttons, value]);
            setValue("");
        }
    }

    function handleDeleteClick(toRemove: string): void {
        setRemoving(toRemove);
    }

    function handleModalClose(): void {
        setRemoving(undefined);
    }

    function handleModalAccept(): void {
        setButtons(buttons.filter((button: string): boolean => button !== removing));
        handleModalClose();
    }

    function renderNewResourceInput(): JSX.Element {
        if (props.token) {
            return <>
                <TextField
                    label="Resource name"
                    variant="outlined"
                    value={value}
                    onChange={handleChange}
                    error={error}
                />
                <Button
                    variant="contained"
                    endIcon={<SendIcon/>}
                    color="success"
                    onClick={handleAdd}
                >Add</Button>
                <br/>
            </>
        }
        return <></>;
    }

    return <div>
        <Button className="navigate" variant="outlined" onClick={() => navigate('/login')}>
            Login &gt;
        </Button>
        <br/>
        {renderNewResourceInput()}
        {
            buttons.map((button: string): JSX.Element => 
                <div key={button}>
                    <ButtonGroup variant="contained">
                        <Button variant="contained" component={Link} to={`/${button}`}>{button}</Button>
                        {props.token && <Button className={`delete-${button}`} onClick={() => handleDeleteClick(button)}>
                            <DeleteForeverIcon/>
                        </Button>}
                    </ButtonGroup>
                </div>
            )
        }
        <Dialog className="dialog" open={removing !== undefined} onClose={handleModalClose}>
            <DialogTitle>Are you sure?</DialogTitle>
            <DialogContent>
                <DialogContentText>
                    You are going to remove "{removing}". Are you sure you want to proceed?
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button onClick={handleModalClose}>Cancel</Button>
                <Button className="accept" onClick={handleModalAccept} autoFocus>Accept</Button>
            </DialogActions>
      </Dialog>
    </div>;
}