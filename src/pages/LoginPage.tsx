import TextField from '@mui/material/TextField';
import React from 'react';
import LoadingButton from '@mui/lab/LoadingButton';
import SendIcon from '@mui/icons-material/Send';
import axios, { AxiosError, AxiosResponse } from 'axios';
import { useNavigate, NavigateFunction } from 'react-router-dom';

export interface LoginPageProps {
    onTokenChange: (token: string) => void;
}

export function LoginPage(props: LoginPageProps): JSX.Element {
    const [login, setLogin] = React.useState<string>("");
    const [password, setPassword] = React.useState<string>("");
    const [loading, setLoading] = React.useState<boolean>(false);
    const [errorCode, setErrorCode] = React.useState<number>(0);
    const navigate: NavigateFunction = useNavigate();

    function handleLoginChange(event: React.ChangeEvent<HTMLInputElement>): void {
        setLogin(event.target.value);
    }

    function handlePasswordChange(event: React.ChangeEvent<HTMLInputElement>): void {
        setPassword(event.target.value);
    }

    function handleLoginClick(): void {
        setLoading(true);
        setErrorCode(0);
        axios.post('https://reqres.in/api/login', {username: login, password})
            .then((response: AxiosResponse<{token: string}>): void => {
                navigate('/');
                props.onTokenChange(response.data.token);
            })
            .catch((error: AxiosError): void => setErrorCode(error.response?.status || 0))
            .finally((): void => setLoading(false));
    }

    function renderError(): string | undefined {
        if (errorCode === 0) {
            return undefined;
        }
        if (errorCode === 400) {
            return "Error: Bad credentials!";
        }
        return "Error: Unknown error";
    }

    return <div>
        <TextField
            className="input-login"
            label="Login"
            variant="outlined"
            fullWidth
            onChange={handleLoginChange}
            value={login}
            disabled={loading}
        /><br/>
         <TextField
            className="input-password"
            label="Password"
            variant="outlined"
            fullWidth
            onChange={handlePasswordChange}
            value={password}
            type="password"
            disabled={loading}
        /><br/>
        {renderError()}
        <br/>
        <LoadingButton
            loadingPosition="end"
            endIcon={<SendIcon />}
            onClick={handleLoginClick}
            variant="contained"
            color="success"
            loading={loading}
        >Login</LoadingButton>
    </div>;
}