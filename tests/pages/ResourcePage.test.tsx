import { mockUseParams } from "../__mocks__/MockReactRouterDom";
import { shallow, ShallowWrapper } from "enzyme";
import React from "react";
import { ResourcePage } from '../../src/pages/ResourcePage'

describe('AddPersonFormPage', (): void => {
    it('renders without resource', (): void => {
        // when
        const wrapper: ShallowWrapper = shallow(<ResourcePage token="test-token"/>);

        // then
        expect(wrapper).toMatchSnapshot();
    });

    it('renders with resource', (): void => {
        // given
        mockUseParams.mockReturnValue({resource: 'test'});

        // when
        const wrapper: ShallowWrapper = shallow(<ResourcePage token="test-token"/>);

        // then
        expect(wrapper).toMatchSnapshot();
    });
});