import '../__mocks__/MockReactRouterDom';
import { shallow, ShallowWrapper } from "enzyme";
import React from "react";
import { Error404Page } from "../../src/pages/Error404Page";

describe('Error404Page', (): void => {
    it('renders', (): void => {
        // when
        const wrapper: ShallowWrapper = shallow(<Error404Page/>);

        // then
        expect(wrapper).toMatchSnapshot();
    });
});