import { shallow, ShallowWrapper } from "enzyme";
import React from "react";
import { BackButton } from "../../src/components/BackButton";

describe('BackButton', (): void => {
    it('renders', (): void => {
        // when
        const wrapper: ShallowWrapper = shallow(<BackButton to="test"/>);

        // then
        expect(wrapper).toMatchSnapshot();
    });
});